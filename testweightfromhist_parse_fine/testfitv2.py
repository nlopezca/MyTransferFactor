import numpy as np
config = {
'axes'      : [
    np.linspace(-5.2,         -1.3,          100 + 1, endpoint=True), # x-axis: rhoDDT 
    np.linspace(np.log(400), np.log(1000), 40 + 1, endpoint=True), # y-axis: log(pT)
    ],

'axes_fine' : [
    np.linspace(-5.2,         -1.3,          10 * 100 + 1, endpoint=True), # x-axis: rhoDDT 
    np.linspace(np.log(400), np.log(1000), 10 * 40 + 1, endpoint=True), # y-axis: log(pT)
    ],

'centres' : [
    ['axes'][0][:-1] + 0.5 * np.diff(['axes'][0]),
    ['axes'][1][:-1] + 0.5 * np.diff(['axes'][1]),
    ],

'centres_fine' : [
    ['axes_fine'][0][:-1] + 0.5 * np.diff(['axes_fine'][0]),
    ['axes_fine'][1][:-1] + 0.5 * np.diff(['axes_fine'][1]),
],

}
print(config['centres_fine'])

import ROOT
inFile = ROOT.TFile.Open("output.root", " READ " )

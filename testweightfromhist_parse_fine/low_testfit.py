import numpy as np
config = {
'axes'      : [
    np.linspace(-5.2,         -1.3,          100 + 1, endpoint=True), # x-axis: rhoDDT 
    np.linspace(np.log(400), np.log(1000), 40 + 1, endpoint=True), # y-axis: log(pT)
    ],

'axes_fine' : [
    np.linspace(-5.2,         -1.3,          10 * 100 + 1, endpoint=True), # x-axis: rhoDDT 
    np.linspace(np.log(400), np.log(1000), 10 * 40 + 1, endpoint=True), # y-axis: log(pT)
    ],

}
config['centres'] = [
    config['axes'][0][:-1] + 0.5 * np.diff(config['axes'][0]),
    config['axes'][1][:-1] + 0.5 * np.diff(config['axes'][1])
]

config['centres_fine'] = [
    config['axes_fine'][0][:-1] + 0.5 * np.diff(config['axes_fine'][0]),
    config['axes_fine'][1][:-1] + 0.5 * np.diff(config['axes_fine'][1])
]

print(config['centres'])

import ROOT
inFile = ROOT.TFile.Open("output_win32_mass100_Sumw2_TH2F.root", " READ " )

hCR = inFile.Get("h_rhovlogpt_ratio_CR")
hSR = inFile.Get("h_rhovlogpt_ratio_SR") 
#print(h)

import root_numpy
from sklearn.gaussian_process import GaussianProcess
import sys, itertools, os, inspect
from multiprocessing import Pool
import time
def predict (clf, data, eval_MSE=None):
    print "data: ", data.shape
    if eval_MSE is None:
        return clf.predict(data)
    return clf.predict(data, eval_MSE)

def batch (iterable, n=1):
    l = len(iterable)
    for ndx in range(0, l, n):
        yield iterable[ndx:min(ndx + n, l)]
        pass
    pass
def asyncPredict (clf, data, num_processes=10, batch_size=100000, quiet=False, eval_MSE=None):
    print "asyncPredict", data.shape
    pool = Pool()
    timeout = 9999999999
    num_examples = data.shape[0]
    num_rounds = int(num_examples/float(num_processes * batch_size)) + 1
    if not quiet: print "Total number of examples: %d (%d is maximal index)" % (num_examples, num_examples - 1)
    predictions = list()
    for iround, round_indices in enumerate(batch(np.arange(num_examples), num_processes * batch_size), start = 1):
        results = list()
        if not quiet: print "asyncPredict: Round %d of %d." % (iround, num_rounds)
        for indices in batch(round_indices, batch_size):

            # Submit prediction as asynchronous process.                                                                                                       
            args = [clf, data[indices,:], eval_MSE]
            if num_processes > 1:
                results.append( pool.apply_async(predict, args) )
            else:
                results.append( predict(*args) )
                pass
            pass

        # Collect predictions.                                                                                                                                 
        if num_processes > 1:
            predictions += [result.get(timeout = timeout) for result in results]
        else:
            predictions += results
            pass
        pass

    # Return predictions.                                                                                                                                      
    return np.hstack(predictions)
#ha= root_numpy.hist2array(h)


X1, X2 = np.meshgrid(*config['centres'])

X = np.vstack((X1.ravel(), X2.ravel())).T

nx, ny = hCR.GetXaxis().GetNbins(), hCR.GetYaxis().GetNbins()

mean = np.zeros((ny,nx))
err  = np.zeros((ny,nx))

for (i,j) in itertools.product(range(nx), range(ny)):
        mean[j,i] = hCR.GetBinContent(i + 1, j + 1) # Transposing due to different ordering
        err [j,i] = hCR.GetBinError  (i + 1, j + 1)
        pass


y = mean.ravel()
s = err.ravel()

msk_fit = (y > 0) 
X_fit = X[msk_fit,:]
y_fit = y[msk_fit]
s_fit = s[msk_fit]

eps = np.finfo(float).eps

nugget = np.square(s_fit/(y_fit + eps)).ravel()


clf = GaussianProcess(theta0=[1E-02, 1E-02], thetaL=[1E-05, 1E-05], thetaU=[1E+01, 1E+01], nugget=nugget)

clf.fit(X_fit, y_fit)

print "  Best value(s) of theta found:", clf.theta_

X3, X4 = np.meshgrid(*config['axes_fine'])
mesh2 = np.vstack((X3.ravel(), X4.ravel())).T

xaxis = config['axes_fine'][0] 
yaxis = config['axes_fine'][1]

#TF_pred, TF_err = clf.predict(mesh2, True)
TF_pred, TF_err = asyncPredict(clf, mesh2, quiet=True, eval_MSE=True, num_processes=1)
#TF_pred = mesh2
#TF_err =mesh2

print("TF_pred")
print(TF_pred)
print("TF_err")
print(TF_err)

TF_pred = TF_pred.reshape(X3.shape)
TF_err  = TF_err .reshape(X3.shape)
h_weight = ROOT.TH2D("h_weight", "h_weight", len(xaxis)-1, -5.2, -1.3, len(yaxis)-1, np.log(400), np.log(1000))
TF_err = np.sqrt(TF_err)

for i in range(len(xaxis)):
    for j in range(len(yaxis)):
       h_weight.SetBinContent(i+1, j+1, TF_pred[j,i])

file = ROOT.TFile("file_parse.root","RECREATE")

h_weight.Write()
file.Close()

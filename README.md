
## Overview

This repository contains scripts for various steps in the data analysis pipeline utilizing the transfer factor method and fitting techniques.

### Repository Contents

- `floattesthistfromroot`: Generates the initial histograms for the transfer factor method.
- `testweightfromhist_parse_fine`: Computes weights using the transfer factor method.
- `makesignal_fullfit_edgeweights_updownerror`: Constructs signal shapes for the final fit.
- `edgetestfitfromweight_fullfit_edgeweights_updownerror`: Performs the full fit.

## Usage

1. **floattesthistfromroot**:
   - Use this script to create initial histograms for the transfer factor method.

2. **testweightfromhist_parse_fine**:
   - Execute this script to calculate weights using the transfer factor method.

3. **makesignal_fullfit_edgeweights_updownerror**:
   - Utilize this script to generate signal shapes for the final fit.

4. **edgetestfitfromweight_fullfit_edgeweights_updownerror**:
   - Run this script to perform the full fit incorporating edge weights and errors.

## Instructions

- Clone the repository to your local environment.
- Navigate to the respective script directory.
- Execute the desired script using appropriate parameters and inputs.
- Ensure dependencies are installed and environment is set up correctly.


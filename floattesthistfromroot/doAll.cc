#include <iostream>
#include "TString.h"
#include "testinputhist.h"
#include "TChain.h"
#include "TFile.h"
#include "TTree.h"
#include "TROOT.h"
#include <string>

using namespace std ;

//void doAll(){
int main(int argc, char* argv[]){
  TString theLink  ;
  TString sigtheLink  ;

  theLink = "/home/nlopezca/testhistfromroot/QCDstorage/*.root";
  sigtheLink += "/home/nlopezca/testhistfromroot/SIGstorage/*175*.root";

  cout << theLink << endl ;
  TChain * myChain = new TChain( "UFO" ) ;
  TTree * myTree ;

  myChain->Add( theLink );
  myChain->Add( sigtheLink );
  cout << "my chain = " << myChain->GetEntries() << endl ;
  
  gROOT->LoadMacro("/home/nlopezca/test_175m/inject1_final_closure/floattesthistfromroot/testinputhist.C+");
  testinputhist * myAnalysis ;
  myAnalysis =  new testinputhist( myChain ) ;
  myAnalysis->Loop();
  
  return 0;
}
